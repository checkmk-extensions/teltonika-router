#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#   _____  __          __  _____
#  / ____| \ \        / / |  __ \
# | (___    \ \  /\  / /  | |__) |
#  \___ \    \ \/  \/ /   |  _  /
#  ____) |    \  /\  /    | | \ \
# |_____/      \/  \/     |_|  \_\
#
# (c) 2022 SWR
# @author Frank Baier <frank.baier@swr.de>
#
# Colors:
#
#                   red
#  magenta                       orange
#            11 12 13 14 15 16
#         46                   21
#         45                   22
#   blue  44                   23  yellow
#         43                   24
#         42                   25
#         41                   26
#            36 35 34 33 32 31
#     cyan                       yellow-green
#                  green
#
# Special colors:
# 51  gray
# 52  brown 1
# 53  brown 2
#
# For a new metric_info you have to choose a color. No more hex-codes are needed!
# Instead you can choose a number of the above color ring and a letter 'a' or 'b
# where 'a' represents the basic color and 'b' is a nuance/shading of the basic color.
# Both number and letter must be declared!
#
# Example:
# "color" : "23/a" (basic color yellow)
# "color" : "23/b" (nuance of color yellow)
#
# As an alternative you can call indexed_color with a color index and the maximum
# number of colors you will need to generate a color. This function tries to return
# high contrast colors for "close" indices, so the colors of idx 1 and idx 2 may
# have stronger contrast than the colors at idx 3 and idx 10.
from cmk.gui.i18n import _
from cmk.gui.plugins.metrics.utils import (
    metric_info,
    graph_info,
)


metric_info["teltonika_satellite_count"] = {
    "title" : _("Satellite count"),
    "unit"  : "count",
    "color" : "21/a",
}

metric_info["teltonika_modem_signal"] = {
    "title" : _("Signal strength (RSSI)"),
    "unit"  : "dbm",
    "color" : "21/a",
}

metric_info["teltonika_modem_sinr"] = {
    "title" : _("Signal-to-Interference-plus-Noise Ratio (SINR)"),
    "unit"  : "db",
    "color" : "21/b",
}

metric_info["teltonika_modem_rsrp"] = {
    "title" : _("Reference Signal Received Power (RSRP)"),
    "unit"  : "dbm",
    "color" : "31/a",
}

metric_info["teltonika_modem_rsrq"] = {
    "title" : _("Reference Signal Received Quality (RSRQ)"),
    "unit"  : "db",
    "color" : "45/a",
}

metric_info["bytes_sent_total"] = {
    "title" : _("Output Octets"),
    "unit"  : "bytes",
    "color" : "#0080e0",
}

metric_info["bytes_received_total"] = {
    "title" : _("Input Octets"),
    "unit"  : "bytes",
    "color" : "#00e060",
}

metric_info["bytes_sent_today"] = {
    "title" : _("Output Octets today"),
    "unit"  : "bytes",
    "color" : "#0080e0",
}

metric_info["bytes_received_today"] = {
    "title" : _("Input Octets today"),
    "unit"  : "bytes",
    "color" : "#00e060",
}

graph_info["teltonika_octets_total"] = {
    "title": _("Data Volume Total"),
    "metrics": [
        ("bytes_received_total", "area"),
        ("bytes_sent_total", "-area"),
    ],
}

graph_info["teltonika_octets"] = {
    "title": _("Data Volume Today"),
    "metrics": [
        ("bytes_received_today", "area"),
        ("bytes_sent_today", "-area"),
    ],
}

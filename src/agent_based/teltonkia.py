#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#   _____  __          __  _____
#  / ____| \ \        / / |  __ \ 
# | (___    \ \  /\  / /  | |__) |
#  \___ \    \ \/  \/ /   |  _  / 
#  ____) |    \  /\  /    | | \ \ 
# |_____/      \/  \/     |_|  \_\
#
# (c) 2023 SWR
# @author Frank Baier <frank.baier@swr.de>
#
# Source: https://wiki.teltonika-networks.com/view/Mobile_Signal_Strength_Recommendations
#
from cmk.gui.i18n import _
from cmk.base.plugins.agent_based.agent_based_api.v1 import (
    check_levels,
    get_value_store,
    register,
    Service,
    Result,
    State,
    SNMPTree,
    contains,
    all_of,
    exists,
    render,
)
from datetime import (
    datetime,
    date
)
from cmk.base.plugins.agent_based.utils.temperature import check_temperature, TempParamType


def discover_teltonika(section):
    yield Service()


def discover_teltonika_gps(section):
    (latitude, longitude, accuracy, datetime, numSatellites) = section[0][0]
    if latitude.lower()!='n/a' and longitude.lower()!='n/a':
        yield Service()


def check_teltonika_device(section):
    (serial, routerName, productCode, batchNumber, hardwareRevision, fwVersion) = section[0][0]

    yield Result(state=State.OK, summary=f"Name: {routerName}")
    yield Result(state=State.OK, summary=f"Serial: {serial}")
    yield Result(state=State.OK, summary=f"Product-Code: {productCode}")
    yield Result(state=State.OK, summary=f"Batch-Number: {batchNumber}")
    yield Result(state=State.OK, summary=f"HW-Rev.: {hardwareRevision}")
    yield Result(state=State.OK, summary=f"FW-Version: {fwVersion}")


def check_teltonika_mobile(section):
    (mIndex, mDescr, mImei, mModel, mManufacturer, mRevision, mSerial, mIMSI, mSimState, mPinState, mNetState,
     mSignal,mOperator, mOperatorNumber, mConnectionState, mConnectionType, mTemperature, mCellID, mSINR, mRSRP,
     mRSRQ, mSent, mReceived, mIP, mSentToday, mReceivedToday, mICCID) = section[0][0]

    if mSimState=='inserted':
        yield Result(state=State.OK, summary=f"SIM: {mSimState}")
    else:
        yield Result(state=State.CRIT, summary=f"SIM: {mSimState}")

    if mPinState=='OK':
        yield Result(state=State.OK, summary=f"PIN: {mPinState}")
    else:
        yield Result(state=State.CRIT, summary=f"PIN: {mPinState}")

    if mNetState.startswith('Registered'):
        if 'home' in mNetState:
            yield Result(state=State.OK, summary=f"Mobile-Network: {mNetState}")
        else:
            yield Result(state=State.WARN, summary=f"Mobile-Network: {mNetState}")
    else:
        yield Result(state=State.CRIT, summary=f"Mobile-Network: {mNetState}")

    yield Result(state=State.OK, summary=f"Operator: {mOperator}")

    if mConnectionState=='Connected':
        yield Result(state=State.OK, summary=f"Con.-State: {mConnectionState}")
    else:
        yield Result(state=State.CRIT, summary=f"Con.-State: {mConnectionState}")

    yield Result(state=State.OK, summary=f"Con.-Type: {mConnectionType}")

    yield Result(state=State.OK, summary=f"IP: {mIP}")
    yield Result(state=State.OK, notice=f"CellID: {mCellID}")
    yield Result(state=State.OK, notice=f"ICCID: {mICCID}")
    yield Result(state=State.OK, notice=f"Modem Description: {mDescr}")
    yield Result(state=State.OK, notice=f"IMEI: {mImei}")
    yield Result(state=State.OK, notice=f"Modem Model: {mModel}")
    yield Result(state=State.OK, notice=f"Modem Manufacturer: {mManufacturer}")
    yield Result(state=State.OK, notice=f"Modem Serial: {mSerial}")
    yield Result(state=State.OK, notice=f"IMSI: {mIMSI}")

    '''
    RSSI for LTE is a calculated from several other signal related measurements: 
    RSSI = wideband power = noise + serving cell power + interference power. 
    For example, a 4G LTE modem might report an RSSI of -68 dBm, but:

    RSRP = -102 dBm
    RSRQ = -16 dB
    SINR = -1.8 dB

    In this case, the signal quality is actually very poor. 
    This could be due to the device being some distance away from the LTE transmitter. 
    It’s also possible that something is interfering with the signal, 
    such as a building or other obstructions between the device and the tower.

    RSSI
    RSSI	Signal strength	Description
    > -65 dBm	        Excellent	Strong signal with maximum data speeds
    -65 dBm to -75 dBm	Good	    Strong signal with good data speeds
    -75 dBm to -85 dBm	Fair	    Fair but useful, fast and reliable data speeds may be attained, 
                                    but marginal data with drop-outs is possible
    -85 dBm to -95 dBm	Poor	    Performance will drop drastically
    <= -95 dBm	        No signal	Disconnection
    '''
    yield from check_levels(
        value=float(mSignal),
        levels_lower=(-80.0, -90.0),
        metric_name="teltonika_modem_signal",
        label=_("Signal strength (RSSI)"),
        render_func=lambda v: f"{v:.0f} dBm",  # pylint: disable=cell-var-from-loop,
        # boundaries=(-100.0, 0),
        notice_only=True,
    )

    '''
    SINR
    SINR    Signal strength	Description
    >= 20 dB	    Excellent	    Strong signal with maximum data speeds
    13 dB to 20 dB	Good	        Strong signal with good data speeds
    0 dB to 13 dB	Fair to poor	Reliable data speeds may be attained, but marginal data with drop-outs is possible.
                                    When this value gets close to 0, performance will drop drastically
    <= 0 dB	        No signal	    Disconnection
    '''
    yield from check_levels(
        value=float(mSINR),
        levels_lower=(5.0, 0.0),
        metric_name="teltonika_modem_sinr",
        label=_("Signal-to-Interference-plus-Noise Ratio (SINR)"),
        render_func=lambda v: f"{v:.0f} dB",  # pylint: disable=cell-var-from-loop,
        # boundaries=(20.0, 0.0),
        notice_only=True,
    )

    '''
    RSRP
    RSRP	Signal strength	Description
    >= -80 dBm	        Excellent	    Strong signal with maximum data speeds
    -80 dBm to -90 dBm	Good	        Strong signal with good data speeds
    -90 dBm to -100 dBm	Fair to poor	Reliable data speeds may be attained, but marginal data with drop-outs is possible.
                                        When this value gets close to -100, performance will drop drastically
    <= -100 dBm	        No signal	    Disconnection
    '''
    yield from check_levels(
        value=float(mRSRP),
        levels_lower=(-90.0, -100.0),
        metric_name="teltonika_modem_rsrp",
        label=_("Reference Signal Received Power (RSRP)"),
        render_func=lambda v: f"{v:.0f} dBm",  # pylint: disable=cell-var-from-loop,
        # boundaries=(0, -100.0),
        notice_only=True,
    )

    '''
    RSRQ
    RSRQ [W] = N × RSRP [W] / RSSI [W] 
    RSRQ [dBm] = 10 x LOG (RB [dBm] x 10^(RSRP [dBm] / 10) / 10^(RSSI [dBm] / 10))
    Excel-Format: RSRQ =10*LOG(RB*10^(RSRP/10)/10^(RSSI/10) 

    RSRQ	Signal quality	Description
    >= -10 dB	        Excellent	    Strong signal with maximum data speeds
    -10 dB to -15 dB	Good	        Strong signal with good data speeds
    -15 dB to -20 dB	Fair to poor	Reliable data speeds may be attained, but marginal data with drop-outs is possible. 
                                        When this value gets close to -20, performance will drop drastically
    <= -20 dB	        No signal	    Disconnection
    '''
    yield from check_levels(
        value=float(mRSRQ),
        levels_lower=(-15.0, -20.0),
        metric_name="teltonika_modem_rsrq",
        label=_("Reference Signal Received Quality (RSRQ)"),
        render_func=lambda v: f"{v:.0f} dB",  # pylint: disable=cell-var-from-loop,
        # boundaries=(-40.0, 0),
        notice_only=True,
    )


def check_teltonika_mobile_traffic(section):
    (mIndex, mDescr, mImei, mModel, mManufacturer, mRevision, mSerial, mIMSI, mSimState, mPinState, mNetState,
     mSignal,mOperator, mOperatorNumber, mConnectionState, mConnectionType, mTemperature, mCellID, mSINR, mRSRP,
     mRSRQ, mSent, mReceived, mIP, mSentToday, mReceivedToday, mICCID) = section[0][0]

    yield from check_levels(
        value=float(mSent) if float(mSent) > 0 else float(mSent) * -1,
        metric_name="bytes_sent_total",
        label=_("Bytes sent total"),
        render_func=render.bytes,
        notice_only=False,
    )

    yield from check_levels(
        value=float(mReceived) if float(mReceived) > 0 else float(mReceived) * -1,
        metric_name="bytes_received_total",
        label=_("Bytes received total"),
        render_func=render.bytes,
        notice_only=False,
    )

    yield from check_levels(
        value=float(mSentToday) if float(mSentToday) > 0 else float(mSentToday) * -1,
        metric_name="bytes_sent_today",
        label=_("Bytes sent today"),
        render_func=render.bytes,
        notice_only=False,
    )

    yield from check_levels(
        value=float(mReceivedToday) if float(mReceivedToday) > 0 else float(mReceivedToday) * -1,
        metric_name="bytes_received_today",
        label=_("Bytes received today"),
        render_func=render.bytes,
        notice_only=False,
    )


def check_teltonika_modem_temperature(params, section):
    (mIndex, mDescr, mImei, mModel, mManufacturer, mRevision, mSerial, mIMSI, mSimState, mPinState, mNetState,
     mSignal,mOperator, mOperatorNumber, mConnectionState, mConnectionType, mTemperature, mCellID, mSINR, mRSRP,
     mRSRQ, mSent, mReceived, mIP, mSentToday, mReceivedToday, mICCID) = section[0][0]

    yield from check_temperature(
        reading=float(mTemperature)/10,
        params=params,
        dev_levels=(70.0, 75.0),
        unique_name=f"modem_temperature",
        value_store=get_value_store(),
    )


def check_teltonika_gps(section):
    (latitude, longitude, accuracy, datetime, numSatellites) = section[0][0]
    yield Result(state=State.OK, notice=f"Lat: {latitude}")
    yield Result(state=State.OK, notice=f"Long: {longitude}")
    yield Result(state=State.OK, notice=f"Accuracy: {accuracy}")
    yield Result(state=State.OK, notice=f"Datetime: {datetime}")

    if numSatellites.lower()!='n/a':
        yield from check_levels(
            value=float(numSatellites),
            metric_name="teltonika_satellite_count",
            label=_("Satellite count"),
            render_func=lambda v: f"{v:.0f}",  # pylint: disable=cell-var-from-loop,
            notice_only=False,
        )


'''
serial 	            .1.3.6.1.4.1.48690.1.1.0 	Device serial number
routerName.0 	    .1.3.6.1.4.1.48690.1.2.0 	Device name
productCode 	    .1.3.6.1.4.1.48690.1.3.0 	Device product (ordering) code
batchNumber 	    .1.3.6.1.4.1.48690.1.4.0 	Device batch number
hardwareRevision 	.1.3.6.1.4.1.48690.1.5.0 	Device hardware revision
fwVersion 	        .1.3.6.1.4.1.48690.1.6.0 	Device RutOS firmware version

[[['6000465338',
   'RUTX50',
   'RUTX5000XXXX',
   '0012',
   '0202',
   'RUTX_R_00.07.04.5']]]
'''
register.snmp_section(
    name="teltonika_device",
    fetch=[
        SNMPTree(
            base=".1.3.6.1.4.1.48690.1",
            oids=["1", "2", "3", "4", "5", "6"]),
    ],
    detect=all_of(
        exists(".1.3.6.1.4.1.48690.1.1.0"), 
        contains(".1.3.6.1.2.1.1.2.0", ".1.3.6.1.4.1.8072.3.2.10"),
    ),
)

register.check_plugin(
    name="teltonika_device",
    sections=["teltonika_device"],
    service_name="Teltonika Device",
    discovery_function=discover_teltonika,
    check_function=check_teltonika_device,
)


'''
mIndex 	            .1.3.6.1.4.1.48690.2.2.1.1.1 	Available modem indexes; used to index OIDs between when the device has multiple modems
mDescr 	            .1.3.6.1.4.1.48690.2.2.1.2.1 	Modem description
mImei 	            .1.3.6.1.4.1.48690.2.2.1.3.1 	Modem IMEI
mModel 	            .1.3.6.1.4.1.48690.2.2.1.4.1 	Modem model
mManufacturer 	    .1.3.6.1.4.1.48690.2.2.1.5.1 	Modem manufacturer
mRevision 	        .1.3.6.1.4.1.48690.2.2.1.6.1 	Modem firmware version
mSerial 	        .1.3.6.1.4.1.48690.2.2.1.7.1 	Modem serial number
mIMSI 	            .1.3.6.1.4.1.48690.2.2.1.8.1 	Modem IMSI number
mSimState 	        .1.3.6.1.4.1.48690.2.2.1.9.1 	SIM card status
mPinState 	        .1.3.6.1.4.1.48690.2.2.1.10.1 	PIN status
mNetState 	        .1.3.6.1.4.1.48690.2.2.1.11.1 	Mobile network registration status
mSignal 	        .1.3.6.1.4.1.48690.2.2.1.12.1 	Signal strength level
mOperator 	        .1.3.6.1.4.1.48690.2.2.1.13.1 	Current mobile network operator
mOperatorNumber     .1.3.6.1.4.1.48690.2.2.1.14.1 	Mobile operator number (MCC+MNC)
mConnectionState    .1.3.6.1.4.1.48690.2.2.1.15.1 	Mobile data connection state
mConnectionType     .1.3.6.1.4.1.48690.2.2.1.16.1 	Mobile data connection type
mTemperature 	    .1.3.6.1.4.1.48690.2.2.1.17.1 	Modem's temperature in 0.1 degrees Celsius
mCellID 	        .1.3.6.1.4.1.48690.2.2.1.18.1 	Cell (Base transceiver station) ID
mSINR 	            .1.3.6.1.4.1.48690.2.2.1.19.1 	SINR value in dB
mRSRP 	            .1.3.6.1.4.1.48690.2.2.1.20.1 	RSRP value in dBm
mRSRQ 	            .1.3.6.1.4.1.48690.2.2.1.21.1 	RSRQ value in dB
mSent 	            .1.3.6.1.4.1.48690.2.2.1.22.1 	Total bytes sent
mReceived 	        .1.3.6.1.4.1.48690.2.2.1.23.1 	Total bytes received
mIP 	            .1.3.6.1.4.1.48690.2.2.1.24.1 	Modem IP address(es)
mSentToday 	        .1.3.6.1.4.1.48690.2.2.1.25.1 	Bytes sent today
mReceivedToday 	    .1.3.6.1.4.1.48690.2.2.1.26.1 	Bytes received today
mICCID 	            .1.3.6.1.4.1.48690.2.2.1.27.1 	SIM ICCID

[[['1',
   '2-1',
   '860302050284123',
   'RG501Q-EU',
   'Quectel',
   'RG501QEUAAR12A08M4G_04.200.04.200',
   '860302050284123',
   '262011905290418',
   'inserted',
   'OK',
   'Registered, home',
   '-56',
   'Telekom.de Telekom.de',
   '26201',
   'Connected',
   '5G-NSA',
   '440',
   '32292608',
   '11',
   '-87',
   '-12',
   '1429783625',
   '-1026033666',
   '37.81.190.228',
   '127340235',
   '1125302518',
   '89490200001789954446']]]
'''
register.snmp_section(
    name="teltonika_mobile",
    fetch=[
        SNMPTree(
            base=".1.3.6.1.4.1.48690.2.2.1",
            oids=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", 
                  "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                  "21", "22", "23", "24", "25", "26", "27"]),
    ],
    detect=all_of(
        exists(".1.3.6.1.4.1.48690.2.2.1.1.1"), 
        contains(".1.3.6.1.2.1.1.2.0", ".1.3.6.1.4.1.8072.3.2.10"),
    ),
)


register.check_plugin(
    name="teltonika_mobile",
    sections=["teltonika_mobile"],
    service_name="Teltonika Mobile",
    discovery_function=discover_teltonika,
    check_function=check_teltonika_mobile,
)


register.check_plugin(
    name="teltonika_mobile_traffic",
    sections=["teltonika_mobile"],
    service_name="Teltonika Mobile Traffic",
    discovery_function=discover_teltonika,
    check_function=check_teltonika_mobile_traffic,
)


register.check_plugin(
    name="teltonika_modem_temperature",
    sections=["teltonika_mobile"],
    service_name="Teltonika Modem Temperature",
    discovery_function=discover_teltonika,
    check_function=check_teltonika_modem_temperature,
    check_ruleset_name="teltonika_temperature",
    check_default_parameters={},
)

'''
GPS 		
latitude 	    .1.3.6.1.4.1.48690.3.1.0 	GPS Latitude value
longitude 	    .1.3.6.1.4.1.48690.3.2.0 	GPS Longitude value
accuracy 	    .1.3.6.1.4.1.48690.3.3.0 	GPS coordinate accuracy
datetime 	    .1.3.6.1.4.1.48690.3.4.0 	GPS coordinate fix time
numSatellites 	.1.3.6.1.4.1.48690.3.5.0 	Number of available GPS satelites
'''
register.snmp_section(
    name="teltonika_gps",
    fetch=[
        SNMPTree(
            base=".1.3.6.1.4.1.48690.3",
            oids=["1", "2", "3", "4", "5"]),
    ],
    detect=all_of(
        exists(".1.3.6.1.4.1.48690.3.1"), 
        contains(".1.3.6.1.2.1.1.2.0", ".1.3.6.1.4.1.8072.3.2.10"),
    ),
)


register.check_plugin(
    name="teltonika_gps",
    sections=["teltonika_gps"],
    service_name="Teltonika GPS",
    discovery_function=discover_teltonika_gps,
    check_function=check_teltonika_gps,
)